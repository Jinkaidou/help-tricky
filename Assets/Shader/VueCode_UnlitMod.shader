Shader "VueCode/UnlitMod" {
	Properties {
		_MainTex ("Texture", 2D) = "white" {}
		_Color ("Main Color", Vector) = (1,1,1,1)
		_Cutoff ("Alpha cutoff", Range(0, 1)) = 0.5
		_Stroke ("Stroke Alpha", Range(0, 1)) = 0.1
		_StrokeColor ("Stroke Color", Vector) = (1,1,1,1)
		_Stroke2 ("Stroke Alpha", Range(0, 1)) = 0.1
		_StrokeColor2 ("Stroke Color", Vector) = (1,1,1,1)
	}
	//DummyShaderTextExporter
	SubShader{
		Tags { "RenderType"="Opaque" }
		LOD 200
		CGPROGRAM
#pragma surface surf Standard
#pragma target 3.0

		sampler2D _MainTex;
		fixed4 _Color;
		struct Input
		{
			float2 uv_MainTex;
		};
		
		void surf(Input IN, inout SurfaceOutputStandard o)
		{
			fixed4 c = tex2D(_MainTex, IN.uv_MainTex) * _Color;
			o.Albedo = c.rgb;
			o.Alpha = c.a;
		}
		ENDCG
	}
}