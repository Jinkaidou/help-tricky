using System.Collections;
using System.Collections.Generic;
using Base.Core.Sound;
using UnityEngine;

[CreateAssetMenu(fileName = "LevelData", menuName = "ScriptableObject/LevelData")]
public class LevelData : ScriptableObject
{
    public ObjectLevel[] objectLevel;
}

[System.Serializable]
public class ObjectLevel
{
    public GameObject levelGO;
    public int id;
    public Sprite spriteLv;
    public Sprite spriteEndLv;
    public AudioClip audioClip = null;
}
