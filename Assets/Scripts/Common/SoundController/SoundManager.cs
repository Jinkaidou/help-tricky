﻿using System.Collections.Generic;
using Controller.LoadData;
using DataAccount;
using UnityEngine;

namespace Base.Core.Sound
{
    public class SoundManager : SingletonMonoDontDestroy<SoundManager>
    {
        public SoundManager(string className) : base(className)
        {
        }

        [SerializeField] private AudioSource musicSource;
        [SerializeField] private AudioSource soundSource;

        private Dictionary<SoundType, AudioClip> _allAudios;

        #region API

        public AudioClip GetAudioClip(SoundType soundType)
        {
            if (_allAudios == null)
            {
                _allAudios = new Dictionary<SoundType, AudioClip>();
            }

            if (_allAudios.ContainsKey(soundType))
            {
                return _allAudios[soundType];
            }

            var audioClip = LoadResourceController.Instance.LoadAudioClip(soundType);
            if (audioClip != null)
            {
                _allAudios[soundType] = audioClip;
            }

            return audioClip;
        }
        
        public AudioClip GetAudioClipLevel(SoundType soundType, int lv)
        {
            if (_allAudios == null)
            {
                _allAudios = new Dictionary<SoundType, AudioClip>();
            }

            if (_allAudios.ContainsKey(soundType))
            {
                return _allAudios[soundType];
            }

            var audioClip = LoadResourceController.Instance.LoadLevelAudioClip(soundType, lv);
            if (audioClip != null)
            {
                _allAudios[soundType] = audioClip;
            }

            return audioClip;
        }

        public void PlayBackgroundMusic(SoundType soundType)
        {
            var audioClip = GetAudioClip(soundType);
            musicSource.clip = audioClip;
            musicSource.Play();
        }

        public void PauseMusic()
        {
            musicSource.Pause();
        }

        public void ResumeMusic()
        {
            musicSource.UnPause();
        }

        public void PlaySoundLevel(SoundType soundType, int level)
        {
            if (DataAccountPlayer.PlayerSettings.SoundOff)
                return;
            
            var audioClip = GetAudioClipLevel(soundType, level);
            soundSource.PlayOneShot(audioClip, soundSource.volume);
        }

        public void PlaySoundEndLevel(AudioClip audioClip)
        {
            soundSource.PlayOneShot(audioClip, soundSource.volume);
        }
        
        public void StopSound()
        {
            this.soundSource.loop = false;
            this.soundSource.clip = null;
            this.soundSource.Stop();
        }
        
        public void PlaySound(SoundType soundType)
        {
            if (!DataAccountPlayer.PlayerSettings.SoundOff)
            {
                var audioClip = GetAudioClip(soundType);
                soundSource.PlayOneShot(audioClip, soundSource.volume);
            }
        }

        public void PlayOneShot(SoundType soundType, float volume = 0.8f)
        {
            if (!DataAccountPlayer.PlayerSettings.SoundOff)
            {
                var audioClip = GetAudioClip(soundType);
                soundSource.PlayOneShot(audioClip, soundSource.volume);
            }
        }

        #endregion

        private void Awake()
        {
            InitData();
            this.RegisterListener(EventID.OnSoundChange, (sender, param) => OnSoundChange());
            this.RegisterListener(EventID.OnMusicChange, (sender, param) => OnMusicChange());
        }

        private void InitData()
        {
            OnSoundChange();
            OnMusicChange();
        }

        private void OnSoundChange()
        {
            soundSource.mute = DataAccountPlayer.PlayerSettings.SoundOff;
        }

        private void OnMusicChange()
        {
            musicSource.mute = DataAccountPlayer.PlayerSettings.MusicOff;
        }
    }
}