using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using DataAccount;

public class PlayerProgress
{
    public HashSet<int> levelsPlay = new();
    public bool isCompleteAll;

    public void AddLevelPLay(int levelId)
    {
        if (!levelsPlay.Contains(levelId))
        {
            levelsPlay.Add(levelId);
            DataAccountPlayer.SavePlayerProgress();
        }
    }

    public int GetLevelMaxPlayed()
    {
        return levelsPlay.Max();
    }

    public void CompleteAllLevel()
    {
        isCompleteAll = true;
        DataAccountPlayer.SavePlayerProgress();
    }
}
