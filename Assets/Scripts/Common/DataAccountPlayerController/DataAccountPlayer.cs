namespace DataAccount
{
    public static class DataAccountPlayer
    {
        private static PlayerSettings _playerSettings;
        private static PlayerTutorialData _playerTutorialData;
        private static PlayerProgress _playerProgress;

        #region Getters
        public static PlayerProgress PlayerProgress
        {
            get
            {
                if (_playerProgress != null)
                {
                    return _playerProgress;
                }

                var playerProgress = new PlayerProgress();
                _playerProgress = ES3.Load(DataAccountPlayerConstants.PlayerProgress, playerProgress);
                return _playerProgress;
            }
        }
        public static PlayerSettings PlayerSettings
        {
            get
            {
                if (_playerSettings != null)
                {
                    return _playerSettings;
                }

                var playerSettings = new PlayerSettings();
                _playerSettings = ES3.Load(DataAccountPlayerConstants.PlayerSettings, playerSettings);
                return _playerSettings;
            }
        }
        public static PlayerTutorialData PlayerTutorialData
        {
            get
            {
                if (_playerTutorialData != null)
                    return _playerTutorialData;

                var playerTutorialData = new PlayerTutorialData();
                _playerTutorialData = ES3.Load(DataAccountPlayerConstants.PlayerTutorialData, playerTutorialData);
                return _playerTutorialData;
            }
        }
        #endregion

        #region Save

        public static void SavePlayerSettings()
        {
            ES3.Save(DataAccountPlayerConstants.PlayerSettings, _playerSettings);
        }

        public static void SavePlayerTutorialData()
        {
            ES3.Save(DataAccountPlayerConstants.PlayerTutorialData, _playerTutorialData);
        }
        
        public static void SavePlayerProgress()
        {
            ES3.Save(DataAccountPlayerConstants.PlayerProgress, _playerProgress);
        }

        #endregion
    }
}