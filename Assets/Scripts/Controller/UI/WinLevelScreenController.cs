using System;
using Base.Core;
using DataAccount;
using Controller.LoadData;
using UnityEngine;
using UnityEngine.UI;
using Base.Core.Sound;

public class WinLevelScreenController : ModuleUIBase
{
    [SerializeField] private Button btnNext;
    [SerializeField] private Button btnEndLevel;
    [SerializeField] private Image imageEnd;
    
    private LevelData _levelData;

    private void Awake()
    {
        _levelData = LoadResourceController.Instance.LoadLevelData();
    }

    private void OnEnable()
    {
        SoundManager.Instance.StopSound();

        /*if (GameManager.currentLevel == 23)
        {
            btnNext.gameObject.SetActive(false);
            btnEndLevel.gameObject.SetActive(true);
        }
        else
        {
            btnNext.gameObject.SetActive(true);
            btnEndLevel.gameObject.SetActive(false);
        }*/

        var levels = _levelData.objectLevel;
        
        for (int i = 0; i < levels.Length; i++)
        {
            if (GameManager.currentLevel == levels[i].id)
            {
                imageEnd.sprite = levels[i].spriteEndLv;

                if (levels[i].audioClip != null)
                {
                    SoundManager.Instance.PlaySoundEndLevel(levels[i].audioClip);
                    //SoundManager.Instance.PlaySound(SoundType.End);
                }
                else
                {
                    SoundManager.Instance.PlaySound(SoundType.End);
                }

                if (levels[i].id == 6)
                {
                    SoundManager.Instance.PlaySoundLevel(SoundType.Camera1, 6);
                    SoundManager.Instance.PlaySoundLevel(SoundType.Camera2, 6);
                }
                
                
                break;
            }
        }
    }

    public override void Start()
    {
        btnNext.onClick.AddListener(OnClickNext);
        btnEndLevel.onClick.AddListener(OnClickEnd);
    }

    private void OnClickNext()
    {
        if (DataAccountPlayer.PlayerProgress.isCompleteAll && GameManager.currentLevel < 23)
        {
            GameManager.currentLevel ++;
        }
        else if (DataAccountPlayer.PlayerProgress.isCompleteAll && GameManager.currentLevel == 23)
        {
            GameManager.currentLevel = 1;
        }
        else
        {
            GameManager.currentLevel = DataAccountPlayer.PlayerProgress.GetLevelMaxPlayed();
        }    
        
        bl_SceneLoaderManager.LoadScene("GameScene");
    }

    private void OnClickEnd()
    {
        bl_SceneLoaderManager.LoadScene("HomeScene");
    }
}
