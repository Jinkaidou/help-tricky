using Base.Core;
using EasyUI.Toast;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelElementController : MonoBehaviour
{
    [SerializeField] private Button buttonSelect;
    [SerializeField] private Text level;
    [SerializeField] private Image imageLv;
    [SerializeField] private GameObject lockGo;

    private ObjectLevel _objectLevel;

    private void Start()
    {
        buttonSelect.onClick.AddListener(OnClickBtnSelect);
    }

    public void InitData(ObjectLevel data)
    {
        _objectLevel = data;
        level.text = data.id.ToString();
        imageLv.sprite = data.spriteLv;
    }

    public void InitPlayerData(HashSet<int> playerProgress)
    {
        if (!playerProgress.Contains(_objectLevel.id))
        {
            lockGo.SetActive(true);
        }
    }

    private void OnClickBtnSelect()
    {
        if (_objectLevel.id > 23)
        {
            Toast.Show("<b>Comming soon</b>", 3f, ToastColor.Blue, ToastPosition.MiddleCenter);
            return;
        }

        GameManager.currentLevel = _objectLevel.id;
        bl_SceneLoaderManager.LoadScene("GameScene");
    }
}
