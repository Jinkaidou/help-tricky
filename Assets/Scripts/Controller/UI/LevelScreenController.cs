using System;
using System.Collections;
using System.Collections.Generic;
using Controller.LoadData;
using Core.Pool;
using DataAccount;
using UnityEngine;
using UnityEngine.UI;

public class LevelScreenController : ModuleUIBase
{
    [SerializeField] private Button buttonClose;
    [SerializeField] private LevelElementController temp;
    [SerializeField] private Transform parent;

    private List<LevelElementController> listLevelEmlement = new();

    private LevelData _levelData;

    private void Awake()
    {
        _levelData = LoadResourceController.Instance.LoadLevelData();
        var levels = _levelData.objectLevel;
        
        for (int i = 0; i < levels.Length; i++)
        {
            var levelElement = Instantiate(temp, parent);
            levelElement.InitData(levels[i]);
            levelElement.gameObject.SetActive(true);
            listLevelEmlement.Add(levelElement);
        }
    }

    private void OnEnable()
    {
        var playerData = DataAccountPlayer.PlayerProgress.levelsPlay;

        foreach (var levelElement in listLevelEmlement)
        {
            levelElement.InitPlayerData(playerData);
        }
    }

    public override void Start()
    {
        buttonClose.onClick.AddListener(OnclickClose);
    }
    
    private void OnclickClose()
    {
        Hide();
    }
}
