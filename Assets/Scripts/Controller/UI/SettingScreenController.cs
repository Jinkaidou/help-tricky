using System.Collections;
using System.Collections.Generic;
using Base.Core;
using DataAccount;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SettingScreenController : ModuleUIBase
{
    [SerializeField] private Button buttonClose;
    [SerializeField] private Button buttonMusic;
    [SerializeField] private Button buttonSound;
    [SerializeField] private Button buttonVibration;
    [SerializeField] private Button buttonSelectLv;
    [SerializeField] private Button buttonHome;
    
    [SerializeField] private GameObject offMusic;
    [SerializeField] private GameObject offSound;
    [SerializeField] private GameObject offVibration;
    [SerializeField] private GameObject onMusic;
    [SerializeField] private GameObject onSound;
    [SerializeField] private GameObject onVibration;
    [SerializeField] private GameObject popupSelectLevel;

    public override void Start()
    {
        buttonClose.onClick.AddListener(OnclickClose);
        buttonMusic.onClick.AddListener(OnclickMusic);
        buttonSound.onClick.AddListener(OnclickSound);
        buttonVibration.onClick.AddListener(OnclickVibration);
        
        offMusic.SetActive(DataAccountPlayer.PlayerSettings.MusicOff);
        offSound.SetActive(DataAccountPlayer.PlayerSettings.SoundOff);
        offVibration.SetActive(DataAccountPlayer.PlayerSettings.VibrationOff);
        
        onMusic.SetActive(!DataAccountPlayer.PlayerSettings.MusicOff);
        onSound.SetActive(!DataAccountPlayer.PlayerSettings.SoundOff);
        onVibration.SetActive(!DataAccountPlayer.PlayerSettings.VibrationOff);

        if(popupSelectLevel != null)
        {
            popupSelectLevel.SetActive(false);
        }

        if(buttonSelectLv != null)
        {
            buttonSelectLv.onClick.AddListener(OnClickSelectLv);
        }
        
        if(buttonHome != null)
        {
            buttonHome.onClick.AddListener(OnClickHome);
        }
    }

    private void OnClickHome() 
    {
        SceneManager.LoadScene(1);
    }

    private void OnClickSelectLv()
    {
        gameObject.SetActive(false);
        popupSelectLevel.transform.GetComponent<LevelScreenController>().Show();
    }
    
    private void OnclickClose()
    {
        Hide();
    }
    
    private void OnclickMusic()
    {
        DataAccountPlayer.PlayerSettings.SetMusic(!DataAccountPlayer.PlayerSettings.MusicOff);
        offMusic.SetActive(DataAccountPlayer.PlayerSettings.MusicOff);
        onMusic.SetActive(!DataAccountPlayer.PlayerSettings.MusicOff);
    }
    
    private void OnclickSound()
    {
        DataAccountPlayer.PlayerSettings.SetSound(!DataAccountPlayer.PlayerSettings.SoundOff);
        offSound.SetActive(DataAccountPlayer.PlayerSettings.SoundOff);
        onSound.SetActive(!DataAccountPlayer.PlayerSettings.SoundOff);
    }
    
    private void OnclickVibration()
    {
        DataAccountPlayer.PlayerSettings.SetVibration(!DataAccountPlayer.PlayerSettings.VibrationOff);
        offVibration.SetActive(DataAccountPlayer.PlayerSettings.VibrationOff);
        onVibration.SetActive(!DataAccountPlayer.PlayerSettings.VibrationOff);
    }
}
