using UnityEngine;
using DG.Tweening;

public class ModuleUIBase : MonoBehaviour
{
    public RectTransform rect;

    public virtual void Start()
    {
        rect.offsetMin = Vector2.zero;
        rect.offsetMax = Vector2.zero;
    }

    public virtual void Show()
    {
        gameObject.SetActive(true);
        transform.localScale = Vector3.zero;
        transform.DOScale(Vector3.one, 0.5f).SetEase(Ease.OutBack);
    }

    public virtual void Hide()
    {
        
        transform.DOScale(Vector3.zero, 0.25f).SetEase(Ease.InBack).OnComplete(() =>
        {
            gameObject.SetActive(false);
        });
    }
}