using Base.Core;
using DataAccount;
using DG.Tweening;
using EasyUI.Toast;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainScreenController : MonoBehaviour
{
    [SerializeField] private Button setting;
    [SerializeField] private GameObject settingPopup;
    [SerializeField] private Button level;
    [SerializeField] private GameObject levelPopup;

    [SerializeField] private Button btnPlay;

    private void Awake()
    {
        Init();
        ButtonListener();
    }

    private void Init()
    {
        settingPopup.SetActive(false);
        levelPopup.SetActive(false);
    }

    private void ButtonListener()
    {
        setting.onClick.AddListener(OnclickSettingBtn);
        level.onClick.AddListener(OnclickLevelBtn);
        btnPlay.onClick.AddListener(OnclickPlayBtn);
    }

    private void OnclickSettingBtn()
    {
        settingPopup.transform.GetComponent<SettingScreenController>().Show();
    }
    
    private void OnclickLevelBtn()
    {
        levelPopup.transform.GetComponent<LevelScreenController>().Show();
    }

    private void OnclickPlayBtn()
    {
        if (DataAccountPlayer.PlayerProgress.isCompleteAll)
        {
            Toast.Show("You have complete all level, Please wait for comming soon", 3f, ToastColor.Blue, ToastPosition.MiddleCenter);
            DOVirtual.DelayedCall(1f, () =>
            {
                levelPopup.transform.GetComponent<LevelScreenController>().Show();
            });

            return;
        }

        GameManager.currentLevel = DataAccountPlayer.PlayerProgress.GetLevelMaxPlayed();
        bl_SceneLoaderManager.LoadScene("GameScene");
    }
}
