using Base.Core.Sound;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundControllerPlay : MonoBehaviour
{
    public SoundType sound;
    public bool loop;

    private void Awake()
    {
        if (loop)
        {
            StartCoroutine(IEAction());
        }
        else
        {
            SoundManager.Instance.PlayOneShot(sound, 0.8f);
        }
    }

    private IEnumerator IEAction()
    {
        while (loop)
        {
            SoundManager.Instance.PlayOneShot(sound, 0.8f);
            Debug.LogWarning("bugggg");
            yield return new WaitForSeconds(1);
        }
    }

    private void OnDisable()
    {
        loop = false;
        StopAllCoroutines();
    }
}
