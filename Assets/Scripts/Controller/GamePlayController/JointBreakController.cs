using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JointBreakController : MonoBehaviour
{
    public ParticleSystem bloodShower;
    public ParticleSystem bloodDrip;

    public bool isBack;
    
    void OnJointBreak2D(Joint2D brokenJoint)
    {
        var blood1 = Instantiate(bloodShower.gameObject, brokenJoint.transform.position, Quaternion.Euler(-150, 90, -120));

        if (isBack)
        {
            blood1 = Instantiate(bloodShower.gameObject, brokenJoint.transform.position, Quaternion.Euler(20, 90, -120));
        }    

        var blood2 = Instantiate(bloodDrip.gameObject, brokenJoint.transform.position, Quaternion.Euler(0, 0, 0));
        blood1.gameObject.SetActive(true);
        blood2.gameObject.SetActive(true);

        blood1.GetComponent<ParticleSystem>().Play();
        blood2.GetComponent<ParticleSystem>().Play();
    }
}
