
using Base.Core.Sound;
using TMPro;
using UnityEngine;

public class CandyController : MonoBehaviour
{
    public string tagRequire;
    public string tagRelease;
    public SoundType sound;
    public int lv;
    public bool isBack = false;
    private bool check;
    private bool checkTag;
    [SerializeField] private ParticleSystem vfxHit;

    private bool shouldMove = false;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.CompareTag(tagRequire) && !shouldMove && !check)
        {
            vfxHit.Play();
            SoundManager.Instance.PlaySoundLevel(sound, lv);
            shouldMove = true;
            var rb = transform.GetComponent<Rigidbody2D>();
            rb.mass = 0;
            rb.bodyType = RigidbodyType2D.Kinematic;

        }

        if (collision.transform.CompareTag(tagRelease))
        {
            check = true;
            checkTag = true;
            var rb = transform.GetComponent<Rigidbody2D>();
            rb.bodyType = RigidbodyType2D.Dynamic;
            rb.mass = 0.01f;
            shouldMove = false;
            this.gameObject.transform.SetParent(null);

            if (isBack)
            {
                transform.position = new Vector3(transform.position.x - 1f, transform.position.y -5.5f, transform.position.z);
                
            } 
            else
            {
                transform.position = new Vector3(transform.position.x + 1.5f, transform.position.y - 2f, transform.position.z);
            } 
        }    
    }

    private void Update()
    {
        if (shouldMove && !checkTag)
        {
            var targetPosition = GameObject.FindGameObjectWithTag(tagRequire).transform;
            this.gameObject.transform.SetParent(targetPosition);
        }
        else if(check && shouldMove == false)
        {
            var rb = transform.GetComponent<Rigidbody2D>();
            rb.bodyType = RigidbodyType2D.Dynamic;
            rb.mass = 0.1f;
        }
    }




}
