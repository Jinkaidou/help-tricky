using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelController2 : MonoBehaviour
{
    public List<GameObject> ObjectsToMove;
    public float TargetJointMaxForce = 500f;
    public LineRenderer lineRendererExample;
    public float lineThickness = 0.1f;
    
    private bool isDragging = false;
    private TargetJoint2D targetJoint2D;
    private Transform capturedObject;
    private LineRenderer lineRenderer;

    private void Start()
	{
	}

    Transform hitPos = null;
    void Update()
    {
#if UNITY_EDITOR
        if (Input.GetMouseButtonDown(0))
        {
            var position = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            var hits = new RaycastHit2D[ObjectsToMove.Count];
            Physics2D.RaycastNonAlloc(position, Vector2.zero, hits);

            foreach (var hit in hits)
            {
                if (hit.transform != null && (hit.transform.parent != null && ObjectsToMove.Contains(hit.transform.parent.gameObject) || ObjectsToMove.Contains(hit.transform.gameObject)))
                {
                    capturedObject = hit.transform;
                    targetJoint2D = capturedObject.gameObject.AddComponent<TargetJoint2D>();
                    targetJoint2D.maxForce = TargetJointMaxForce;

                    hitPos = hit.collider.transform;

                    targetJoint2D.anchor = hitPos.localPosition;

                    isDragging = true;

                    lineRenderer = Instantiate(lineRendererExample, capturedObject.position, Quaternion.identity);
                    lineRenderer.startWidth = lineThickness;
                    lineRenderer.endWidth = lineThickness;
                    lineRenderer.positionCount = 2;

                    lineRenderer.SetPosition(0, hitPos.position);
                    lineRenderer.SetPosition(1, targetJoint2D.target);
                    lineRenderer.enabled = true;

                    if (capturedObject.GetComponent<Rigidbody2D>().bodyType == RigidbodyType2D.Kinematic)
                    {
                        capturedObject.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;
                    }

                    if (ObjectsToMove.Contains(hit.transform.parent.gameObject))
                    {
                        if (capturedObject.parent.gameObject.GetComponent<Rigidbody2D>().bodyType == RigidbodyType2D.Kinematic)
                        {
                            capturedObject.parent.gameObject.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;
                        }
                    }

                    break;
                }
            }
        }

        if (Input.GetMouseButton(0) && isDragging)
        {
            if (capturedObject != null)
            {
                var position = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                targetJoint2D.target = position;
                lineRenderer.SetPosition(0, hitPos.position);
                lineRenderer.SetPosition(1, position);
            }
        }

        if (Input.GetMouseButtonUp(0))
        {
            if (capturedObject != null)
            {
                var body = capturedObject.GetComponent<Rigidbody2D>();
                if (body != null /*&& body.bodyType == RigidbodyType2D.Dynamic*/)
                {
                    body.velocity = Vector3.zero;
                    body.angularVelocity = 0f;
                }

                capturedObject = null;
            }

            if (targetJoint2D != null)
            {
                Destroy(targetJoint2D);
            }

            isDragging = false;
            if (lineRenderer != null)
            {
                lineRenderer.enabled = false;
            }
        }
#endif

        if (Input.touchCount > 0)
        {
            if (Input.GetTouch(0).phase == TouchPhase.Began)
            {
                var position = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                var hits = new RaycastHit2D[ObjectsToMove.Count];
                Physics2D.RaycastNonAlloc(position, Vector2.zero, hits);

                foreach (var hit in hits)
                {
                    if (hit.transform != null && (hit.transform.parent != null && ObjectsToMove.Contains(hit.transform.parent.gameObject) || ObjectsToMove.Contains(hit.transform.gameObject)))
                    {
                        capturedObject = hit.transform;
                        targetJoint2D = capturedObject.gameObject.AddComponent<TargetJoint2D>();
                        targetJoint2D.maxForce = TargetJointMaxForce;

                        hitPos = hit.collider.transform;

                        targetJoint2D.anchor = hitPos.localPosition;

                        isDragging = true;

                        lineRenderer = Instantiate(lineRendererExample, capturedObject.position, Quaternion.identity);
                        lineRenderer.startWidth = lineThickness;
                        lineRenderer.endWidth = lineThickness;
                        lineRenderer.positionCount = 2;

                        lineRenderer.SetPosition(0, hitPos.position);
                        lineRenderer.SetPosition(1, targetJoint2D.target);
                        lineRenderer.enabled = true;

                        if (capturedObject.GetComponent<Rigidbody2D>().bodyType == RigidbodyType2D.Kinematic)
                        {
                            capturedObject.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;
                        }

                        if (ObjectsToMove.Contains(hit.transform.parent.gameObject))
                        {
                            if (capturedObject.parent.gameObject.GetComponent<Rigidbody2D>().bodyType == RigidbodyType2D.Kinematic)
                            {
                                capturedObject.parent.gameObject.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;
                            }
                        }

                        break;
                    }
                }
            }

            if (Input.GetTouch(0).phase == TouchPhase.Moved && isDragging)
            {
                if (capturedObject != null)
                {
                    var position = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                    targetJoint2D.target = position;
                    lineRenderer.SetPosition(0, hitPos.position);
                    lineRenderer.SetPosition(1, position);
                }
            }

            if (Input.GetTouch(0).phase == TouchPhase.Ended || Input.GetTouch(0).phase == TouchPhase.Canceled)
            {
                if (capturedObject != null)
                {
                    var body = capturedObject.GetComponent<Rigidbody2D>();
                    if (body != null)
                    {
                        body.velocity = Vector3.zero;
                        body.angularVelocity = 0f;
                    }

                    capturedObject = null;
                }

                if (targetJoint2D != null)
                {
                    Destroy(targetJoint2D);
                }

                isDragging = false;
                if (lineRenderer != null)
                {
                    lineRenderer.enabled = false;
                }
            }
        }

    }
}
