using Base.Core.Sound;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class XiLanhController : MonoBehaviour
{
    public string tagRequire;

    public GameObject pushColider;
    public GameObject push;
    public GameObject pushVisual;

    public List<GameObject> pullList;

    public bool canPlaySound;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag(tagRequire))
        {
            if (!canPlaySound)
            {
                canPlaySound = true;
                SoundManager.Instance.PlaySound(SoundType.PickCandy);
            }
            pushVisual.SetActive(true);
            pushColider.SetActive(true);
            push.GetComponent<SliderJoint2D>().enabled = true;
            push.GetComponent<FixedJoint2D>().enabled = false;

            foreach (var obj in pullList)
            {
                obj.SetActive(false);
            }

            this.transform.parent.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Static;
           
        }
    }
}
