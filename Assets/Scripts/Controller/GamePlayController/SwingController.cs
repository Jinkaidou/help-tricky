using UnityEngine;

public class SwingController : MonoBehaviour
{
    public float swingForce = 5f;    // The force to swing the object
    public float rotationLimit = 45f;    // The maximum rotation angle

    private bool isSwingingRight = true;    // Flag to track the swinging direction

    private void FixedUpdate()
    {
        if (isSwingingRight)
        {
            // Apply the swing force clockwise
            GetComponent<Rigidbody2D>().AddTorque(-swingForce);

            // Limit the rotation to the right
            if (transform.rotation.eulerAngles.z > rotationLimit && transform.rotation.eulerAngles.z < 180f)
                isSwingingRight = false;
        }
        else
        {
            // Apply the swing force counterclockwise
            GetComponent<Rigidbody2D>().AddTorque(swingForce);

            // Limit the rotation to the left
            if (transform.rotation.eulerAngles.z < 360f - rotationLimit && transform.rotation.eulerAngles.z > 180f)
                isSwingingRight = true;
        }
    }
}