using Base.Core.Sound;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerController : MonoBehaviour
{
    public SoundType soundTrigger;
    public SoundType soundComplete;
    public string tagRequire;
    public int amountRequire;
    private int _count;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.name.Contains(tagRequire))
        {
            _count += 1;
            SoundManager.Instance.PlaySound(soundTrigger);
            if (_count < amountRequire)
            {
                this.PostEvent(EventID.CreateNextObject, _count);
                collision.gameObject.name = "aaaa";
            }
            if (_count >= amountRequire)
            {
                collision.gameObject.name = "aaaa";
                StartCoroutine(ShowWin());

                if(soundComplete != SoundType.None)
                {
                    SoundManager.Instance.PlaySound(soundComplete);
                }    
            }    
        }
    }

    IEnumerator ShowWin()
    {
        yield return new WaitForSeconds(1f);
        this.PostEvent(EventID.Win);
    }

    private void OnDisable()
    {
        StopAllCoroutines();
    }
}
