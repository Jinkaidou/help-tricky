using Base.Core.Sound;
using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MosquitoController : MonoBehaviour
{
    public string tagRequire;
    [SerializeField] private ParticleSystem vfxHit;
    [SerializeField] private GameObject mosquito;
    [SerializeField] private GameObject mosquitoDie;

    [SerializeField] private SoundType sound;
    [SerializeField] private SoundType sound2;

    
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.name.Contains(tagRequire))
        {
            SoundManager.Instance.PlaySoundLevel(sound, 2);

            tagRequire = "aaa";
            mosquito.gameObject.SetActive(false);
            mosquitoDie.gameObject.SetActive(true);

            DOVirtual.DelayedCall(0.75f, () =>
            {
                SoundManager.Instance.PlaySoundLevel(sound2, 2);
                vfxHit.Play();
                StartCoroutine(ShowWin());
            });
        }
    }

    IEnumerator ShowWin()
    {
        yield return new WaitForSeconds(3f);
        this.PostEvent(EventID.Win);
    }   
}
