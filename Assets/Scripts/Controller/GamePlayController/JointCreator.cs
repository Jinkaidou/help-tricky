using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JointCreator : MonoBehaviour
{
    [SerializeField] private List<GameObject> allBones;

    [ContextMenu("CreatJoints")]
    private void CreatJoint()
    {
        allBones = new List<GameObject>();
        GetBone(transform);

        Rigidbody2D previousRB = null;

        for (int i = 0; i < allBones.Count; i++)
        {
            allBones[i].AddComponent<HingeJoint2D>();
            
            if(previousRB != null)
                allBones[i].GetComponent<HingeJoint2D>().connectedBody = previousRB;
            
            previousRB = allBones[i].GetComponent<Rigidbody2D>();
        }

        for (int i = 0; i < allBones.Count; i++)
        {
            allBones[i].AddComponent<SpringJoint2D>();

            if (previousRB != null)
                allBones[i].GetComponent<SpringJoint2D>().connectedBody = previousRB;

            previousRB = allBones[i].GetComponent<Rigidbody2D>();
        }
    }

    private void GetBone(Transform bone)
    {
        allBones.Add((bone.gameObject));

        foreach (Transform child in bone)
        {
            GetBone(child);
        }
    }
}
