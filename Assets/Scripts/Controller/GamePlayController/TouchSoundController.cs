using System;
using Base.Core.Sound;
using UnityEngine;

public class TouchSoundController : MonoBehaviour
{
    public SoundType sound;
    public int level;
    private void OnMouseDown()
    {
        SoundManager.Instance.PlaySoundLevel(sound, level);
    }
}
