using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisableFreezeController : MonoBehaviour
{
    private Rigidbody2D _rb;

    private void Awake()
    {
        _rb = GetComponent<Rigidbody2D>();
    }

    private void OnMouseDown()
    {
        OnDisableFreeze();
    }



    private void OnDisableFreeze()
    {
        _rb.bodyType = RigidbodyType2D.Dynamic;
        _rb.freezeRotation = false;
    }
}
