using Base.Core;
using Com.LuisPedroFonseca.ProCamera2D;
using DG.Tweening;
using System.Collections;
using UnityEngine;

public class InteractiveZoomController : MonoBehaviour
{
    private GameObject startScene;
    private GameObject playScene;
    private GameObject Level;
    private bool checkOpen;

    private void OnEnable()
    {
        checkOpen = true;
        Level = GameObject.Find("Level " + GameManager.currentLevel + "(Clone)").gameObject;
        startScene = Level.transform.GetChild(0).gameObject;
        playScene  = Level.transform.GetChild(1).gameObject;
    }

    private void OnMouseDown()
    {
        if (checkOpen)
        {
            ZoomStory();
            this.PostEvent(EventID.EnteringGame);
        }
    }

    public void ZoomStory()
    {
        ProCamera2D.Instance.Zoom(-15, 2);
        DOVirtual.DelayedCall(1.5f, () =>
        {
            startScene.SetActive(false);
            playScene.SetActive(true);
            ProCamera2D.Instance.Zoom(11.5f, 2);
            checkOpen = false;
        });
           
    }


}
