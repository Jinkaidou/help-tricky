
using Base.Core.Sound;
using DG.Tweening;
using UnityEngine;

public class TeethFoodController : MonoBehaviour
{
    public string tagRequire;
    public string tagActive;
    public string tagRelease;
    public GameObject GoCanActive;
    public SoundType sound1, sound2;
    public int lv1, lv2;
    public bool isSpecial;
    public bool isAbove;
    


    private bool canActive = false;
    private bool check;
    [SerializeField] private ParticleSystem vfxHit;

    private bool shouldMove = false;

    private void Awake()
    {
        var rb = transform.GetComponent<Rigidbody2D>();
        rb.mass = 0;
        rb.bodyType = RigidbodyType2D.Kinematic;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.CompareTag(tagActive) && !canActive)
        {
            canActive = true;
            SoundManager.Instance.PlaySoundLevel(sound1, lv1);
            vfxHit.Play();
            GoCanActive.SetActive(true);
            transform.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;

            if (isSpecial)
            {
                var newPos = new Vector3(transform.position.x, transform.position.y + 0.5f, transform.position.z);
                transform.DOJump(newPos, 0.2f, 1, 0.2f);
                transform.rotation = Quaternion.Euler(0, 0, 0);
            }  
            
            if(isAbove)
            {
                var newPos = new Vector3(transform.position.x, transform.position.y - 0.5f, transform.position.z);
                transform.position = newPos;
                transform.rotation = Quaternion.Euler(0, 0, 0);
            }
           
        }

        if (collision.transform.CompareTag(tagRequire) && canActive)
        {
            //vfxHit.Play();
            SoundManager.Instance.PlaySoundLevel(sound2, lv2);
            shouldMove = true;

            var rb = transform.GetComponent<Rigidbody2D>();
            rb.mass = 0;
            rb.bodyType = RigidbodyType2D.Kinematic;
        }

        if (collision.transform.CompareTag(tagRelease))
        {
            check = true;
            var rb = transform.GetComponent<Rigidbody2D>();
            rb.bodyType = RigidbodyType2D.Dynamic;
            rb.mass = 1000;
            shouldMove = false;
            transform.position = new Vector3(transform.position.x - 2.5f, transform.position.y - 2.5f, transform.position.z);
        }    
    }

    private void Update()
    {
        if (shouldMove)
        {
            var targetPosition = GameObject.FindGameObjectWithTag(tagRequire).transform;
            this.gameObject.transform.SetParent(targetPosition);
            //transform.position = Vector3.MoveTowards(transform.position, targetPosition, Time.fixedDeltaTime * 5f);
        }
        else if (check && !shouldMove)
        {
            var rb = transform.GetComponent<Rigidbody2D>();
            rb.bodyType = RigidbodyType2D.Dynamic;
            rb.mass = 1000;
        }
    }




}
