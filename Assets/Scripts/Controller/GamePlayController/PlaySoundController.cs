using System;
using System.Collections;
using System.Collections.Generic;
using Base.Core.Sound;
using UnityEngine;

public class PlaySoundController : MonoBehaviour
{
    public SoundType sound;
    public bool loop;
    public int level;
    public int timeLoop;

    private void OnEnable()
    {
        if (loop)
        {
            StartCoroutine(IEAction());
        }
        else
        {
            SoundManager.Instance.PlaySoundLevel(sound, level);
        }
    }

    private IEnumerator IEAction()
    {
        while (loop)
        {
            SoundManager.Instance.PlaySoundLevel(sound, level);
            yield return new WaitForSeconds(timeLoop);
        }
    }

    private void OnDisable()
    {
        SoundManager.Instance.StopSound();
    }
}
