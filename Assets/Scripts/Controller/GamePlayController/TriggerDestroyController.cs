using Base.Core.Sound;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerDestroyController : TriggerController
{

    private int _countInherrit;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.name.Contains(tagRequire))
        {
            _countInherrit += 1;
            Debug.Log("count" + _countInherrit);
            SoundManager.Instance.PlaySound(soundTrigger);
            if (_countInherrit < amountRequire)
            {
                this.PostEvent(EventID.CreateNextObject, _countInherrit);
                collision.gameObject.name = "aaaa";
            }
            if (_countInherrit >= amountRequire)
            {
                collision.gameObject.name = "aaaa";
                StartCoroutine(ShowWin());

                if (soundComplete != SoundType.None)
                {
                    SoundManager.Instance.PlaySound(soundComplete);
                }

            }
        }
    }

    IEnumerator ShowWin()
    {
        yield return new WaitForSeconds(1f);
        this.PostEvent(EventID.Win);
    }

    private void OnDisable()
    {
        StopAllCoroutines();
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.name.Contains(tagRequire))
        {
            Destroy(collision.gameObject);
        }
    }
}
