using Base.Core.Sound;
using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnTTController : MonoBehaviour
{
    public string tagRequire;
    public int number;
    public GameObject obSpawn;
    public Transform spawnPos;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.name.Contains(tagRequire))
        {
            Instantiate(obSpawn, spawnPos.position, Quaternion.identity);

            for (int i = 1; i < number; i++)
            {
                StartCoroutine(Spawn());
               
            }
        }
    }

    private IEnumerator Spawn()
    {
        yield return new WaitForSeconds(0.1f);
        Instantiate(obSpawn, spawnPos.position, Quaternion.identity);
        SoundManager.Instance.PlayOneShot(SoundType.BugMoveing1, 0.3f);
    }    
}
