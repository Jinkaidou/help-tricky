using System;
using UnityEngine;

public class ApplyForce : MonoBehaviour
{
    [SerializeField] private Vector2 force;
    private Rigidbody2D _rb2D;

    private void Awake()
    {
        _rb2D = GetComponent<Rigidbody2D>();
    }

    private void OnEnable()
    {
        ApplyForceToObject(force);
    }

    private void ApplyForceToObject(Vector2 forceee)
    {
        if (_rb2D != null)
        {
            _rb2D.AddForce(forceee);
        }
    }

    private float _time = 0;
    private void Update()
    {
        if (_time <= 0.3f)
        {
            _time += Time.deltaTime;
        }
        else
        {
            force = new Vector2(-force.x, force.y);
            ApplyForceToObject(force);
            _time = 0;
        }
    }
}
