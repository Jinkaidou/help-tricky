using Base.Core.Sound;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BugControllerSound : MonoBehaviour
{
    private bool check;

    private void Start()
    {
        check = true;
        StartCoroutine(IEAction());
    }

    private IEnumerator IEAction()
    {
        while (check)
        {
            var num = Random.Range(2, 5);
            SoundManager.Instance.PlayOneShot(SoundType.BugMoveing1, 0.3f);

            Debug.LogWarning("bugggg");
            yield return new WaitForSeconds(num);
        }
    }

    private void OnDisable()
    {
        check = false;
        StopAllCoroutines();
    }
}
