using Base.Core;
using Base.Core.Sound;
using Controller.LoadData;
using DataAccount;
using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    public Transform levelContainer;
    [SerializeField] private Button btnPause;
    [SerializeField] private Button btnSkip;
    [SerializeField] private Button btnRePlay;
    [SerializeField] private GameObject popupPause;
    [SerializeField] private GameObject popupWin;
    [SerializeField] private GameObject bgIngame;
    [SerializeField] private GameObject bgSplash;

    private void Awake()
    {
        bgIngame.SetActive(false);
        bgSplash.SetActive(true);

        var levelData = LoadResourceController.Instance.LoadLevelData();
        var levels = levelData.objectLevel;

        foreach (var level in levels)
        {
            if (level.id == GameManager.currentLevel)
            {
                Instantiate(level.levelGO, levelContainer);
            }    
        }

        InitUI();
    }

    private void InitUI()
    {
        popupPause.SetActive(false);
    }

    private void Start()
    {
        btnPause.onClick.AddListener(OnclickPause);
        btnSkip.onClick.AddListener(OnclickSkipLevel);
        btnRePlay.onClick.AddListener(OnclickRePlay);

        this.RegisterListener(EventID.Win, (sender, param) =>
        {
            popupWin.SetActive(true);

            if (GameManager.currentLevel == DataAccountPlayer.PlayerProgress.GetLevelMaxPlayed())
            {
                if (DataAccountPlayer.PlayerProgress.GetLevelMaxPlayed() < 23)
                {
                    DataAccountPlayer.PlayerProgress.AddLevelPLay(GameManager.currentLevel + 1);
                }

                if (GameManager.currentLevel == 23 && !DataAccountPlayer.PlayerProgress.isCompleteAll)
                {
                    DataAccountPlayer.PlayerProgress.CompleteAllLevel();
                }    
            }
                
        });

        this.RegisterListener(EventID.EnteringGame, (sender, param) =>
        {
            DOVirtual.DelayedCall(1.5f, () =>
            {
                bgIngame.SetActive(true);
                bgSplash.SetActive(false);
                
                btnPause.gameObject.SetActive(false);
                btnRePlay.gameObject.SetActive(true);
            });
          
        });
    }

    private void OnclickPause()
    {
        popupPause.transform.GetComponent<SettingScreenController>().Show();
    }
    
    private void OnclickRePlay()
    {
        bl_SceneLoaderManager.LoadScene("GameScene");
    }

    private void OnclickSkipLevel()
    {
        //ads
        popupWin.SetActive(true);
        if (GameManager.currentLevel == DataAccountPlayer.PlayerProgress.GetLevelMaxPlayed())
        {
            DataAccountPlayer.PlayerProgress.AddLevelPLay(GameManager.currentLevel + 1);
        }
    }
}
