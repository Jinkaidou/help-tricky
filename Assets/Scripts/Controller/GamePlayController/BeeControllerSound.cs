using Base.Core.Sound;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class BeeControllerSound : MonoBehaviour
{
    private bool check;

    private int load;

    private void Awake()
    {
        check = true;
        StartCoroutine(IEAction());
    }

    private IEnumerator IEAction()
    {
        while (load < 3)
        {
            load +=  1;
            SoundManager.Instance.PlayOneShot(SoundType.BeeSound, 0.8f);
            Debug.LogWarning("bugggg");
            yield return new WaitForSeconds(1);
        }
    }

    private void OnDisable()
    {
        check = false;
        StopAllCoroutines();
    }

    private void OnDestroy()
    {
        check = false;
        StopAllCoroutines();
    }
}
