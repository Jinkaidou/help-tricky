using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnController : MonoBehaviour
{
    public List<GameObject> interactiveObject;
    private void Awake()
    {
        this.RegisterListener(EventID.CreateNextObject, (sender, param) => CreatObject((int) param));
    }

    private void OnEnable()
    {
        for (int i = 0; i < interactiveObject.Count; i++)
        {
            interactiveObject[i].SetActive(false);
        }

        interactiveObject[0].SetActive(true);
    }
    private void CreatObject(int count)
    {
        interactiveObject[count].SetActive(true);
    }
}
