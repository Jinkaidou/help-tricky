﻿
namespace Controller.LoadData
{
    public static class ResourcesFolderPath
    {
        //Data

        // tên của biến dùng để trỏ đến DATA = // tên folder của Data đó
        public const string DataFolder = "DATA/{0}/";
        public const string DataFolderFood = "Foods";
        public const string DataFolderHero = "Heros";
        public const string DataFolderItem = "Items";
        public const string DataFolderName = "Names";
        public const string DataFolderSpawnData = "Spawns";
        public const string DataFolderPercentSpawnData = "PercentSpawn";
        public const string DataFolderStatUpgrade = "StatUpgradeData";

        public const string UiFolder = "UI/";
        public const string SoundFolder = "SOUNDS/";
        public const string IconFolder = "ICONS";
        public const string HeroFolder = "HeroIO";
        public const string SoundLevelFolder = "Sounds 1/lv_{0}/";
    }
}

