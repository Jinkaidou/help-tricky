using UnityEngine;

public class TouchHandler : MonoBehaviour
{
    public LineRenderer lineRenderer;
    public float lineThickness = 0.1f;

    private Rigidbody2D rb;
    private SpringJoint2D springJoint;

    private bool isDragging = false;

    public bool interaction;

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        springJoint = GetComponent<SpringJoint2D>();

        lineRenderer.startWidth = lineThickness;
        lineRenderer.endWidth = lineThickness;
        lineRenderer.positionCount = 2;
    }

    private void Update()
    {

        if (Input.GetMouseButtonDown(0)) // Use Input.GetTouch(0).phase == TouchPhase.Began for touch input
        {
            RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);

            if (hit.collider != null && hit.collider.gameObject == gameObject)
            {
                rb.bodyType = RigidbodyType2D.Dynamic;
                springJoint.enabled = true;
                springJoint.connectedAnchor = Camera.main.ScreenToWorldPoint(Input.mousePosition);

                isDragging = true;
                lineRenderer.SetPosition(0, transform.position);
                lineRenderer.SetPosition(1, springJoint.connectedAnchor);
                lineRenderer.enabled = true;
            }
        }
        else if (Input.GetMouseButton(0) && isDragging) // Use Input.GetTouch(0).phase == TouchPhase.Moved for touch input
        {
            springJoint.connectedAnchor = Camera.main.ScreenToWorldPoint(Input.mousePosition);

            lineRenderer.SetPosition(0, transform.position);
            lineRenderer.SetPosition(1, springJoint.connectedAnchor);
        }
        else if (Input.GetMouseButtonUp(0)) // Use Input.GetTouch(0).phase == TouchPhase.Ended or TouchPhase.Canceled for touch input
        {
            springJoint.enabled = false;
            isDragging = false;
            lineRenderer.enabled = false;
        }
    }
}
