public enum EventID
{
    EarnGold,
    EatFood,
    EnemyDie,
    PlayerDie,
    WasEat,
    LevelUp,
    EatItem,
    RespawnEnemy,
    ShieldHit,
    EarnMoney,
    SpendMoney,
    BlockSide,
    MovingAgain,


    // Settings
    OnSoundChange,
    OnMusicChange,
    OnVibrationChange,

    //UI
    DailyCheckInClaimReward,
    Win,
    CreateNextObject,
    EnteringGame,
}